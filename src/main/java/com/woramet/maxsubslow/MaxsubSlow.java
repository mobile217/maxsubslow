/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.woramet.maxsubslow;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class MaxsubSlow {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int m = 0;
        int n = 10;
        int num_list[] = new int[n];
        
        for (int i = 0; i < n ; i++){
            num_list[i] = kb.nextInt();
        }
        System.out.print("Input : ");
        System.out.println(Arrays.toString(num_list));
        
        
        m = MaxsubSlow(n, num_list, m);
        System.out.println("Output : " + m);
    }
    
    private static int MaxsubSlow(int n, int[] num_list, int m) {
        for (int j = 0; j < n; j++){
            for(int k = j; k < n; k++){
                int s = 0;
                for (int i = j; i <= k; i++){
                    s = s + num_list[i];
                    if(s > m){
                        m = s;
                    }
                }
            }
        }
        return m;
    }
}
